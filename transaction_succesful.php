<?php
session_start();
?>
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <link rel="stylesheet" href="CSS/main.css" type="text/css">
    <meta charset="UTF-8">
    <title>PHP PGSQL transactions</title>
    <script>
      function goBack() {
        window.location.pathname = "/"
      }
    </script>
  </head>
  <body>
    <div class="donemsg">
      <p>
        <?php
          $from = $_SESSION["from"];
          $to = $_SESSION["to"];
          $sum = $_SESSION["sum"];
          echo "$from on siirtänyt $sum euroa henkilölle $to.";
        ?>
      </p>
      <br>
      <button onclick="goBack()">Back to transactions.</button>
    </div>
  </body>
</html>
