<?php
session_start();

$_SESSION["from"] = NULL;
$_SESSION["to"] = NULL;
$_SESSION["sum"] = NULL;

$passwordfile = fopen("password.txt", "r");
$password = fgets($passwordfile);
fclose($passwordfile);

$dbconn = pg_connect("host=localhost dbname=kurssi user=phpserver password=$password")
  or die('Could not connect: ' . pg_last_error());
$state_q = 'SELECT * FROM tili ORDER BY omistaja';
$state_result = pg_query($state_q) or die('Query failed: ' . pg_last_error());
$column_titles = array("Tilinro", "Omistaja", "Summa (€)");

function transaction($sum, $from_acc, $to_acc) {
  if ($from_acc == $to_acc) {
    echo "Lähettäjä ja vastaanottaja eivät voi olla samat.";
    return False;
  }
    
  pg_query("BEGIN") or die("Siirtoa ei voitu aloittaa\n");
  
  $res_removed = pg_query_params("
    UPDATE tili
    SET summa = summa - $1
    WHERE tilinumero = $2 AND summa > $1;
    ", array($sum, $from_acc));

  $res_added = pg_query_params("
    UPDATE tili
    SET summa = summa + $1
    WHERE tilinumero = $2;
    ", array($sum, $to_acc));

  if (pg_affected_rows($res_removed) != 1) {
    echo "Peruutetaan siirto. Lähettäjän tilillä ei tarpeeksi rahaa, tilinumero väärin tai muu virhe.";
    pg_query("ROLLBACK") or die("Peruutus epäonnistui\n");
    return False;
  }

  if (pg_affected_rows($res_added) != 1) {
    echo "Peruutetaan siirto. Vastaanottajan tilinumero saattaa olla väärin.";
    pg_query("ROLLBACK") or die("Peruutus epäonnistui\n");
    return False;
  }
   
  if ($res_removed and $res_added) {
    pg_query("COMMIT") or die("Siirron varmistus epäonnistui\n");

    // Set session variables 
    $res_f_name = pg_query_params("
      SELECT omistaja FROM tili
      WHERE tilinumero = $1
    ", array($from_acc));
    $res_t_name = pg_query_params("
      SELECT omistaja FROM tili
      WHERE tilinumero = $1
    ", array($to_acc));
    while ($line = pg_fetch_array($res_f_name, null, PGSQL_ASSOC)) {
      $_SESSION["from"] = reset($line);
    }
    while ($line = pg_fetch_array($res_t_name, null, PGSQL_ASSOC)) {
      $_SESSION["to"] = reset($line);
    }
    $_SESSION["sum"] = $sum;

    return True;
  }
}
function display_result() {
  if (transaction($_POST["amount"],$_POST["from_acc"],$_POST["to_acc"])) {
    header( 'Location: /transaction_succesful.php' );
    exit();
  }
}

?>

<!DOCTYPE HTML>
<html lang="en">
  <head>
    <link rel="stylesheet" href="CSS/main.css" type="text/css">
    <meta charset="UTF-8">
    <title>PHP PGSQL transactions</title>
  </head>
  <body>
    <div class="formContainer">
      <form id="transaction_form" method="post">
        <label>Lähettäjän tilinumero</label><br>
        <input type="text" name="from_acc" required><br>
        <label>Vastaanottajan tilinumero</label><br>
        <input type="text" name="to_acc" required><br>
        <label>Siirrettävä summa (€)</label><br>
        <input type="text" name="amount" required><br>
        <input type="submit" name"submit_btn" value="Siirrä rahaa"><br>
        <p class="errortxt">
        <?php
          if($_SERVER['REQUEST_METHOD']=='POST') {
            display_result();
          } 
        ?>
        </p>

    </div>
    <div class="databaseState">
      <p>Tietokannan tila:</p>
      <?php
        echo "<table>\n";
        echo "<tr class='theader'>";
        foreach($column_titles as $title)
          echo "<td>$title</td>";
        echo "</tr>";
        while ($line = pg_fetch_array($state_result, null, PGSQL_ASSOC)) {
          echo "\t<tr>\n";
          foreach ($line as $col_value) {
            echo "\t\t<td>$col_value</td>\n";
          }
          echo "\t</tr>\n";
        }
        echo "</table>\n";
      ?>
    </div>
  </body>
</html>
